import { TextField, Button } from '@mui/material';
import React, { useState } from 'react';
import { useQuery } from 'react-query';
import { useNavigate } from 'react-router-dom';
import Cookies from 'universal-cookie';
import { makeStyles } from '@mui/styles';

const useStyles = makeStyles({
  title: {
    color: '#333',
    textAlign: 'center',
    textTransform: 'uppercase',
    fontFamily: 'Bebas Neue',
    fontSize: '50px',
    margin: '0 0 50px 0 !important'
  },
  loginInput: {
    width: '100%',
    margin: '0 0 20px 0 !important'
  },
  loginBtn: {
    width: '50%',
    margin: '30px 25% 0 25% !important',
    borderRadius: '20px',
    display: 'block',
    textAlign: 'center',
    background:
      'linear-gradient(123deg, rgba(253,29,29,1) 0%, rgba(252,176,69,1) 100%)',
    color: '#fff !important',
    fontWeight: 'bold !important',
    transition: '0.4s ease',
    '&:hover': {
      background:
        'linear-gradient(123deg, rgba(252,176,69,1) 0%, rgba(253,29,29,1) 100%)'
    }
  },
  loginCard: {
    width: '58%',
    margin: '30px auto !important',
    padding: '11%',
    borderRadius: '15px',
    boxShadow: '0 12px 40px -12.125px rgba(0,0,0,0.3)'
  }
});

const Login = () => {
  const classes = useStyles();

  const navigate = useNavigate();
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [errorMessage, setErrorMessage] = useState();
  const { refetch } = useQuery(
    'repoData',
    () => fetch(`api/login/?username=${username}&password=${password}`)
      .then((res) => res.json())
      .then((data) => {
        if (!data.message) {
          const cookies = new Cookies();
          cookies.set('loged', username, { path: '/' });
          navigate(`/dashboard/${username}`);
        } else {
          setErrorMessage(data.message);
        }
      }),
    { enabled: false }
  );

  const HandleSearch = () => {
    refetch();
  };

  return (
    <>
      <div className={classes.loginCard}>
        <h1 className={classes.title}>Login</h1>
        <TextField
          className={classes.loginInput}
          label="username"
          onChange={(e) => setUsername(e.target.value)}
          value={username}
          color="warning"
        />
        <TextField
          className={classes.loginInput}
          label="password"
          onChange={(e) => setPassword(e.target.value)}
          value={password}
          color="warning"
        />
        {errorMessage && <p>{errorMessage}</p>}
        <Button onClick={HandleSearch} className={classes.loginBtn}>
          Login
        </Button>
      </div>
    </>
  );
};

export default Login;
