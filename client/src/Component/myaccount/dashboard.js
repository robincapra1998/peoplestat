import { Typography, Grid } from '@mui/material';
import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import Donut from 'react-donut';
import { useQuery } from 'react-query';
import { makeStyles } from '@mui/styles';
import * as React from 'react';

const useStyles = makeStyles({
  donut: {
    width: '100%',
    textAlign: 'center',
    padding: '2%'
  },
  charts: {
    display: 'flex!important',
    width: '50%',
    margin: '100px 5%'
  },
  userImg: {
    width: '60px',
    display: 'inline-block',
    borderRadius: '100%',
    marginRight: '20px'
  },
  align: {
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    flexWrap: 'wrap',
    margin: 'O 20% !important',
    justifyContent: 'center'
  },
  username: {
    fontSize: '34px !important',
    marginLeft: '10px'
  },
  totaluser: {
    fontSize: '24px',
    marginLeft: '10px'
  }

});
const arr = [];
const Dashboard = () => {
  const classes = useStyles();

  const { username } = useParams();
  const [countryStat, setCountryStat] = useState();

  const { data: user } = useQuery('user', () => fetch(`http://localhost:3000/user/${username}`).then((res) => res.json()));
  const { data: totalCount } = useQuery('totalCount', () => fetch('http://localhost:3000/api/getTotalCount').then((res) => res.json()));
  const { data: gender } = useQuery('gender', () => fetch('http://localhost:3000/api/genderStat').then((res) => res.json()));

  useEffect(() => {
    fetch('http://localhost:3000/api/countryStat')
      .then((response) => response.json())
      .then((data) => {
        Object.keys(data.data).map((e, i) => (arr.push({
          name: Object.keys(data.data)[i],
          data: Object.values(data.data)[i]
        })));

        setCountryStat(arr);
      });
  }, []);
  return (
    <>
      {user && totalCount && gender && countryStat && (
        <div>
          <div className={classes.align}>
            <img
              alt="userpic"
              src={user.result[0].picture.medium}
              className={classes.userImg}
            />
            <div>
              <Typography className={classes.username}>
                {user.result[0].name.first + user.result[0].name.last}
              </Typography>
              <Typography className={classes.totaluser}>
                Total users : {totalCount.data}
              </Typography>
            </div>
          </div>

              <Grid container direction={'row'}>
                <Grid item>
                  <Donut
                    className={classes.center}
                    chartData={countryStat}
                    chartWidth={750}
                    chartHeight={750}
                    title="Country stat"
                    legendAlignment="left"
                    chartThemeConfig={{
                      title: {
                        color: 'red',
                        fontFamily: 'Bebas Neue !important',
                        fontWeight: 'bold'
                      },
                      chart: {
                        fontFamily: 'Bebas Neue !important'
                      }
                    }}
                  /></Grid>
                <Grid item >
                  {gender && (
                    <Donut
                      className={classes.donut}
                      chartData={[
                        { name: 'female', data: gender.female },
                        { name: 'male', data: gender.male }
                      ]}
                      chartWidth={750}
                      chartHeight={750}
                      title="Gender Stat"
                      legendAlignment="left"
                      chartThemeConfig={{
                        chart: {
                          fontFamily: 'Bebas Neue !important'
                        },
                        title: {
                          color: 'red',
                          fontFamily: 'Bebas Neue !important',
                          fontWeight: 'bold'
                        },
                        series: {
                          colors: ['#ff00cc', '#00ccff']
                        }
                      }}
                    />
                  )}</Grid>
              </Grid>
        </div>
      )}
    </>

  );
};

export default Dashboard;
