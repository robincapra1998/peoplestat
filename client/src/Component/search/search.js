import { TextField, Button } from '@mui/material';
import { useState, React } from 'react';
import { makeStyles } from '@mui/styles';
import SearchResult from './searchResult';

const useStyles = makeStyles({
  searchInput: {
    width: '48%',
    margin: '15px 1% !important'
  },
  searchBtn: {
    width: '98%',
    padding: '15px !important',
    margin: '15px 1% !important',
    borderRadius: '20px',
    display: 'block',
    textAlign: 'center',
    background:
      'linear-gradient(123deg, rgba(253,29,29,1) 0%, rgba(252,176,69,1) 100%)',
    color: '#fff !important',
    fontWeight: 'bold !important',
    transition: '0.4s ease',
    '&:hover': {
      background:
        'linear-gradient(123deg, rgba(252,176,69,1) 0%, rgba(253,29,29,1) 100%)'
    }
  }
});

const Search = () => {
  const classes = useStyles();

  const [gender, setgender] = useState('');
  const [city, setcity] = useState('');
  const [state, setstate] = useState('');
  const [phone, setphone] = useState('');
  const [firstname, setfirstname] = useState('');
  const [lastname, setlastname] = useState('');
  const [query, setquery] = useState('');

  const HandleSearch = () => {
    setquery(true);
  };
  return (
    <>
      <TextField
        className={classes.searchInput}
        label="gender"
        id="fullWidth"
        onChange={(e) => setgender(e.target.value)}
        value={gender}
        color="warning"
      />
      <TextField
        className={classes.searchInput}
        label="state"
        id="fullWidth"
        onChange={(e) => setstate(e.target.value)}
        value={state}
        color="warning"
      />
      <TextField
        className={classes.searchInput}
        label="city"
        id="fullWidth"
        onChange={(e) => setcity(e.target.value)}
        value={city}
        color="warning"
      />
      <TextField
        className={classes.searchInput}
        label="phone"
        id="fullWidth"
        onChange={(e) => setphone(e.target.value)}
        value={phone}
        color="warning"
      />
      <TextField
        className={classes.searchInput}
        label="firstname"
        id="fullWidth"
        onChange={(e) => setfirstname(e.target.value)}
        value={firstname}
        color="warning"
      />
      <TextField
        className={classes.searchInput}
        label="lastname"
        id="fullWidth"
        onChange={(e) => setlastname(e.target.value)}
        value={lastname}
        color="warning"
      />
      <Button onClick={HandleSearch} className={classes.searchBtn}>
        Search
      </Button>

      {query && (
        <SearchResult
          gender={gender}
          city={city}
          state={state}
          phone={phone}
          firstname={firstname}
          lastname={lastname}
          query={query}
        />
      )}
    </>
  );
};

export default Search;
