import React from 'react';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import { useNavigate } from 'react-router-dom';
import { makeStyles } from '@mui/styles';

import { useQuery } from 'react-query';

const useStyles = makeStyles({
  searchCard: {
    width: '98%',
    margin: '30px 1% !important',
    display: 'inline-block',
    transition: '0.3s',
    '&:hover': {
      boxShadow: '0 12px 40px -12.125px rgba(0,0,0,0.3)'
    }
  },
  searchCardContent: {
    display: 'flex',
    alignItems: 'center',
    flexWrap: 'wrap'
  },
  img: {
    borderRadius: '100%',
    minHeight: '200px',
    marginRight: '35px'
  },
  btn: {
    background:
      'linear-gradient(123deg, rgba(253,29,29,1) 0%, rgba(252,176,69,1) 100%)',
    color: '#fff !important',
    fontWeight: 'bold !important',
    width: '180px',
    marginRight: '0 !important',
    transition: '0.4s ease',
    '&:hover': {
      background:
        'linear-gradient(123deg, rgba(252,176,69,1) 0%, rgba(253,29,29,1) 100%)'
    }
  },
  cardAction: {
    padding: '0 !important'
  }
});

const SearchResult = ({
  gender,
  city,
  state,
  phone,
  firstname,
  lastname
}) => {
  const classes = useStyles();
  const navigate = useNavigate();
  const { isLoading, error, data } = useQuery('repoData', () => fetch(
    `api/searchOne/?firstname=${firstname}&lastname=${lastname}&gender=${gender}&state=${state}&city=${city}&phone=${phone}`
  ).then((res) => res.json()));
  if (isLoading) return 'Loading...';

  if (error) return `An error has occurred: ${error.message}`;

  const handleClick = (username) => {
    navigate(`/user/${username}`);
  };

  return data.response.map((user) => (
    <>
      <Card sx={{ minWidth: 275 }} className={classes.searchCard}>
        <CardContent className={classes.searchCardContent}>
          <img
            alt="userpic"
            src={user.picture.large}
            className={classes.img}
          ></img>
          <div>
            <Typography
              sx={{ mb: 1.4 }}
              fontSize={26}
              onClick={() => handleClick(user.login.username)}
            >
              {user.name.gender} {user.name.title} {user.name.first}
              {user.name.last}
            </Typography>
            <Typography sx={{ mb: 1.5 }} color="text.secondary">
              {user.email}
            </Typography>
            <Typography sx={{ mb: 1.5 }} color="text.secondary">
              {user.gender}
            </Typography>
            <Typography sx={{ mb: 1.5 }} color="text.secondary">
              {user.phone}
            </Typography>
            <Typography sx={{ mb: 1.5 }} color="text.secondary">
              {user.cell}
            </Typography>
            <CardActions className={classes.cardAction}>
              <Button
                size="medium"
                className={classes.btn}
                onClick={() => handleClick(user.login.username)}
              >
                Learn More
              </Button>
            </CardActions>
          </div>
        </CardContent>
      </Card>
    </>
  ));
};

export default SearchResult;
