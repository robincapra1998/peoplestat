import { useParams } from 'react-router-dom';
import { useState, React } from 'react';
import Typography from '@mui/material/Typography';
import { useQuery } from 'react-query';
import { makeStyles } from '@mui/styles';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';

const useStyles = makeStyles({
  searchCard: {
    width: '50%',
    margin: '30px 25% !important',
    display: 'inline-block',
    boxShadow: '0 12px 40px -12.125px rgba(0,0,0,0.3) !important'
  },
  searchCardContent: {
    display: 'flex',
    alignItems: 'center',
    flexWrap: 'wrap'
  },
  img: {
    borderRadius: '100%',
    height: '300px',
    margin: '3%'
  }
});

const User = () => {
  const classes = useStyles();
  const { username } = useParams();
  const [user, setUser] = useState();
  const { isLoading, error } = useQuery('repoData', () => fetch(`${username}`)
    .then((res) => res.json())
    .then((data) => setUser(data.result[0])));
  if (isLoading) return 'Loading...';

  if (error) return `An error has occurred: ${error.message}`;

  return (
    <>
      {user && (
        <>
          <Card sx={{ minWidth: 275 }} className={classes.searchCard}>
            <CardContent className={classes.searchCardContent}>
              <img alt="userpic" src={user.picture.large} className={classes.img}></img>
              <div>
                <Typography
                  sx={{ fontSize: 14 }}
                  color="text.secondary"
                  gutterBottom
                ></Typography>
                <Typography sx={{ mb: 1.5 }} variant="h5" component="div">
                  {user.name.title} {user.name.first} {user.name.last}
                </Typography>
                <Typography sx={{ mb: 1.5 }} color="text.secondary">
                  Gender : {user.gender}
                </Typography>
                <Typography sx={{ mb: 1.5 }} color="text.secondary">
                  Email : {user.email}
                </Typography>
                <Typography sx={{ mb: 1.5 }} color="text.secondary">
                  Home Phone : {user.phone}
                </Typography>
                <Typography sx={{ mb: 1.5 }} color="text.secondary">
                  Cell Phone : {user.cell}
                </Typography>
                <Typography sx={{ mb: 1.5 }} color="text.secondary">
                  Date of birth : {user.dob.date.slice(0, 10)} ({user.dob.age}{' '}
                  y.o.)
                </Typography>
                <Typography sx={{ mb: 1.5 }} color="text.secondary">
                  Member since {user.registered.date.slice(0, 10)} (
                  {user.registered.age} years)
                </Typography>
              </div>
            </CardContent>
          </Card>
        </>
      )}
    </>
  );
};

export default User;
