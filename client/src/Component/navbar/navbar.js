import { useQuery } from 'react-query';
import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';

import { makeStyles } from '@mui/styles';

const useStyles = makeStyles({

  userImg: {
    width: '60px',
    display: 'inline-block',
    borderRadius: '100%',
    marginRight: '20px'
  },
  dashboard: {
    marginRight: '25px',
    color: 'white',
    textDecoration: 'none'
  },
  link: {
    textDecoration: 'none',
    color: 'white'
  },
  navbar: {
    background:
            'linear-gradient(123deg, rgba(253,29,29,1) 0%, rgba(252,176,69,1) 100%)',
    color: '#fff !important'
  }
});

const NavBar = ({ username }) => {
  const classes = useStyles();
  const { data: user, isLoading } = useQuery('user', () => fetch(`http://localhost:3000/user/${username}`).then((res) => res.json()));

  if (isLoading) {
    return <div>Loading...</div>;
  }
  const dashboardLink = `http://localhost:3000/dashboard/${username}`;
  return (
        <>
            <Box sx={{ flexGrow: 1 }}>
                <AppBar position="static" className={classes.navbar}>
                    <Toolbar>
                        <IconButton
                            size="large"
                            edge="start"
                            color="inherit"
                            aria-label="menu"
                            sx={{ mr: 2 }}
                        >
                            <AccountCircleIcon />
                        </IconButton>
                        <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                            {`${user.result[0].name.first} ${user.result[0].name.last}`}

                        </Typography>
                        <a href={dashboardLink} className={classes.dashboard}> DashBoard </a>
                        <a href="http://localhost:3000/search" className={classes.link}> Search</a>
                    </Toolbar>
                </AppBar>
            </Box>
        </>
  );
};

export default NavBar;
