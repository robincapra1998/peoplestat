import React, { useState, useEffect } from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { makeStyles } from '@mui/styles';
import Cookies from 'universal-cookie';
import { QueryClient, QueryClientProvider } from 'react-query';
import Button from '@mui/material/Button';
import Login from './Component/login/login';
import Search from './Component/search/search';
import User from './Component/user/user';
import Dashboard from './Component/myaccount/dashboard';
import NavBar from './Component/navbar/navbar';

const queryClient = new QueryClient();

const useStyles = makeStyles({
  error: {
    fontWeight: 'bold',
    fontSize: '50px'
  },
  title: {
    color: 'rgba(253,29,29,1)',
    textAlign: 'center',
    textTransform: 'uppercase',
    fontFamily: 'Bebas Neue',
    fontSize: '60px'
  },
  container: {
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'column',
    alignItems: 'center'
  },
  btn: {
    marginTop: '70px!important',
    background:
      'linear-gradient(123deg, rgba(253,29,29,1) 0%, rgba(252,176,69,1) 100%)',
    color: '#fff !important',
    fontWeight: 'bold !important',
    marginRight: '0 !important',
    transition: '0.4s ease',
    width: '30%',
    '&:hover': {
      background:
        'linear-gradient(123deg, rgba(252,176,69,1) 0%, rgba(253,29,29,1) 100%)'
    }
  }
});

const App = () => {
  const classes = useStyles();
  const [cookie, setCookie] = useState();
  useEffect(() => {
    const cookies = new Cookies();
    setCookie(cookies.get('loged'));
  }, [cookie]);

  return (
    <div className="wrapper">
      <QueryClientProvider client={queryClient}>
        {cookie && <NavBar username={cookie} />}
        <BrowserRouter>
          <h1 className={classes.title}>People Stats</h1>
          <Routes>
            {cookie ? (
              <>
                <Route path="search" element={<Search />}></Route>
                <Route path="user/:username" element={<User />}></Route>
                <Route path="login" element={<Login />}></Route>
                <Route
                  path="dashboard/:username"
                  element={<Dashboard />}
                ></Route>
                <Route
                  path="*"
                  element={
                    <p className={classes.container} >
                      <p className={classes.error}>404 Nothing to see here</p>
                      <Button href={`/dashboard/${cookie}`} className={classes.btn}>retourné au dashboard</Button>
                    </p>
                  }
                ></Route>
              </>
            ) : (
              <>
                <Route
                  path="*"
                  element={
                    <p>
                      Vous devez vous connecter<a href="/login"> ICI</a>
                      <br></br>
                      Si vous vous etes connecter rafraichissez la page
                    </p>
                  }
                ></Route>
                <Route path="login" element={<Login />}></Route>
              </>
            )}
          </Routes>
        </BrowserRouter>
      </QueryClientProvider >
    </div >
  );
};

export default App;
