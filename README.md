# PeopleStat



## Getting started

People Stat is a school project we will use a node Backend and a react frontend see futher for installation

## Install Package

Navigate to Client and run npm i to install deps

`cd client/ && npm i`

do the same things in server 

`cd server/ && npm i`

## Compile and run the front-end

Navigate to Client and run npm start 

`cd client/ && npm start`

## Compile and run the back-end

Navigate to server and run npm start 

`cd server/ && npm start`

## Details

The Api will run on port 3001 and the front will run on 3002, make sure to install dependancies

We use material UI librairy so you need to be connected to internet to load things
